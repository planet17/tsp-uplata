<?php
return [
    'host'     => 'localhost',
    'database' => 'db_name',
    'username' => 'postgres',
    'password' => '',
    'port'     => 5432,
];
