<?php
return [
    'defaultTarget' => 'forumod',
    'forumsConfig' => [
        'forumod' => [
            'crawlerMethodClass' => \App\Crawlers\ForumOd\CrawlerMethod::class,
            'accountHelperClass' => \App\Crawlers\ForumOd\AccountHelper::class,
            'targetTopicUrl' => 'showthread.php?t=501406',
            'login'   => [
                'username' => 'my_first_acc_here',
                'password' => 'my_first_pass_here',
            ],
            'pages' => [
                'limit' => 10
            ]
        ]
    ],
];
