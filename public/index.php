<?php

use App\App;

require_once dirname(__DIR__) . '/vendor/autoload.php';

try {
    (new App())->bootstrap()->parse('forumod');
} catch (\Exception $e) {
    die($e->getMessage());
}
