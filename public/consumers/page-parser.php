<?php

use App\App;
use App\Queue\Consumers\PageParser;

require_once dirname(__DIR__,2) . '/vendor/autoload.php';

(new App())->bootstrap()->initConsumer(PageParser::class);
