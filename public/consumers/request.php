<?php

use App\App;
use App\Queue\Consumers\Request;

require_once dirname(__DIR__,2) . '/vendor/autoload.php';

(new App())->bootstrap()->initConsumer(Request::class);
