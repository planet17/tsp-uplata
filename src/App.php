<?php


namespace App;


use App\Services\Crawlers\ForumTopic\CrawlerFactory;
use App\Services\DB;
use App\Services\MessageQueue\RabbitMQ\Connection as MQ;
use App\Services\MessageQueue\RabbitMQ\Interfaces\ConsumerInterface;
use Exception;


class App
{
    private $config = [];

    public function bootstrap():App
    {
        $this->loadConfig();
        $this->configureServices();

        return $this;
    }

    /**
     *
     * @param null $forumConfigTarget
     *
     * @throws Exception
     */
    public function parse($forumConfigTarget = null):void
    {
        !$forumConfigTarget && $forumConfigTarget = $this->config['forum']['defaultTarget'];
        if (!$forumConfigTarget) {
            throw new \RuntimeException('Undefined forum configuration for parse');
        }

        $config = $this->config['forum']['forumsConfig'][$forumConfigTarget];
        $builder = (new CrawlerFactory())
            ->setCrawlerMethodClass($config['crawlerMethodClass'])
            ->setTargetTopicUrl($config['targetTopicUrl']);
        if (array_key_exists('pages', $config) && !empty($config['pages'])
           && array_key_exists('limit', $config['pages'])
           && !empty($config['pages']['limit'])) {
            $builder->setPagesLimit($config['pages']['limit']);
        }

        if (
            array_key_exists('accountHelperClass', $config) && !empty($config['accountHelperClass'])
            && array_key_exists('login', $config)
            && count($config['login'])
        ) {
            $builder->setAccountHelperClass($config['accountHelperClass']);
            $builder->setUserLogin($config['login']['username'] ?? '');
            $builder->setUserPassword($config['login']['password'] ?? '');
        }



        $builder->factory()->run();
    }

    /**
     * @param string $consumerClass
     */
    public function initConsumer(string $consumerClass):void
    {
        if (!is_a($consumerClass, ConsumerInterface::class, true)) {
            throw new \RuntimeException('Provided class not implement: ' . ConsumerInterface::class);
        }

        /** @var ConsumerInterface $consumer */
        $consumer = new $consumerClass();

        $consumer->consume();
    }

    private function configureServices():void
    {
        $this->configureDB();
        $this->configureMQ();
    }

    private function configureDB():void
    {
        /**
         * $var string $host
         * $var string $database
         * $var string $username
         * $var string $password
         * $var integer $port
         */
        extract($this->config['db'], null);
        DB::setConfig($username, $password, $database, $host, $port);
    }

    private function configureMQ():void
    {
        /**
         * $var string $host
         * $var string $username
         * $var string $password
         * $var integer $port
         */
        extract($this->config['queue'], null);
        MQ::setCredentials($username, $password, $host, $port);
    }

    private function loadConfig():void
    {
        $directory             = dirname(__DIR__) . '/config/';
        $this->config['db']    = require_once $directory . 'db.php';
        $this->config['queue'] = require_once $directory . 'queue.php';
        $this->config['forum'] = require_once $directory . 'forum.php';
    }
}


if ( !function_exists('ddTrace')) {
    function ddTrace(...$item)
    {
        $docRoot = __DIR__ . DIRECTORY_SEPARATOR;
        foreach ($item as $i) {
            echo gettype($i) . ':' ;
            print_r($i);
            echo str_repeat(PHP_EOL, 2);
        }

        $file = str_replace(dirname($docRoot), '', debug_backtrace()[0]['file']);
        $line = debug_backtrace()[0]['line'];
        $length = strlen("{$file}:{$line}");
        $underline = str_repeat('-', $length);
        $above = str_repeat('>', $length/18*17);
        echo("\n\n{$above}\n{$file}:{$line}\n{$underline}\n");
        die;
    }
}
