<?php


namespace App\Builders;


use App\Entity\Post;


class PostBuilder
{
    /** @var Post */
    protected $post;

    public function __construct()
    {
        $this->create();
    }

    public function create():PostBuilder
    {
        $this->post = new Post();

        return $this;
    }

    public function setTopicTitle(string $val):PostBuilder
    {
        $this->post->topicTitle = $val;

        return $this;
    }

    public function setBody(string $val):PostBuilder
    {
        $this->post->body =  $val;

        return $this;
    }

    public function setAuthor(string $val):PostBuilder
    {
        $this->post->author = $val;

        return $this;
    }

    public function setDate(string $val):PostBuilder
    {
        $this->post->date = $val;

        return $this;
    }

    public function get():Post
    {
        $post = $this->post;
        $this->create();

        return $post;
    }
}
