<?php


namespace App\Entity;


/**
 * Class Post
 *
 * @package App\Entity
 * @property string $topicTitle
 * @property string $body
 * @property string $author
 * @property string $date
 */
class Post extends Entity
{
    public $topicTitle;
    public $body;
    public $author;
    public $date;
}
