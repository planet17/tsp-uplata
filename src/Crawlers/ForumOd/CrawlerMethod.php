<?php


namespace App\Crawlers\ForumOd;


use App\Queue\Jobs\RequestJob;
use App\Services\Crawlers\ForumTopic\CrawlerMethod as Base;
use Exception;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\DomCrawler\Crawler;


class CrawlerMethod extends Base
{
    protected $linkBase = 'https://forumodua.com/';

    protected $isLogin  = true;


    /**
     * @inheritDoc
     */
    public function validateLoginResponseStatus():void
    {
        if ($this->loginResponse->getStatusCode() !== 200) {
            throw new Exception('Wrong login response status code: ' . $this->loginResponse->getStatusCode());
        }
    }

    /**
     * @inheritDoc
     */
    public function validateLoginResponseBody():void
    {
        $content = $this->extractBodyFromResponse($this->loginResponse);
        $crawler = new Crawler();
        $crawler->addHtmlContent($content, $this->getCharset());
        $crawlerMsg = $crawler->filter('div.standard_error form.block.vbform div.blockbody .blockrow.restore');

        if ( !$crawlerMsg->count()) {
            throw new Exception('Login failed: not found elements with successful message.');
        }

        $needle = $this->forgeHelloPhrase($this->accountHelper->getUserLogin());
        $check  = $crawlerMsg->html();
        if ($check !== $needle) {
            throw new Exception('Login failed: string "' . $check . '" not equals to template: "' . $needle . '"');
        }
    }

    private function forgeHelloPhrase(string $name):string
    {
        return "Спасибо, что зашли, {$name}.";
    }

    protected function dispatchRequestProcessor(RequestInterface $request):void
    {
        (new RequestJob($this, $request))->dispatch();
    }
}
