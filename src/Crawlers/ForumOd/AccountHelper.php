<?php


namespace App\Crawlers\ForumOd;

use App\Services\Crawlers\ForumTopic\AccountHelper as Base;


class AccountHelper extends Base
{
    protected $formActionLink = '/login.php?do=login';

    public function forgeLoginFormData():array
    {
        return [
            'vb_login_username'        => $this->getUserLogin(),
            'vb_login_password'        => '',
            'vb_login_password_hint'   => '(unable to decode value)',
            'cookieuser'               => '1',
            'securitytoken'            => 'guest',
            'do'                       => 'login',
            'vb_login_md5password'     => md5($this->getUserPassword()),
            'vb_login_md5password_utf' => md5($this->getUserPassword()),
        ];
    }
}
