<?php


namespace App\Services\MessageQueue\RabbitMQ;


use App\Services\MessageQueue\RabbitMQ\Interfaces\ConsumerInterface;
use App\Services\MessageQueue\RabbitMQ\Interfaces\JobInterface;
use App\Services\MessageQueue\RabbitMQ\Interfaces\TaskInterface;
use ErrorException;
use PhpAmqpLib\Message\AMQPMessage;
use RuntimeException;

class Worker extends Queueable implements ConsumerInterface
{
    protected $message;

    /**
     * @throws ErrorException
     */
    public function consume():void
    {
        $this->channelDeclaration();
        $this->getChannel()->basic_consume(
            $this->getQueueName(),
            '',
            false,
            true,
            false,
            false,
            [$this, 'callHandle']
        );

        while ($this->getChannel()->is_consuming()) {
            $this->getChannel()->wait();
        }
    }

    public function handle(TaskInterface $task)
    {
        throw new RuntimeException('That method must be override at custom worker.');
    }

    final public function callHandle(AMQPMessage $message)
    {
        $this->message = $message;
        $task          = unserialize($this->message->body, [TaskInterface::class, JobInterface::class]);
        // @todo add hook
        try {
            if ($task instanceof JobInterface) {
                $task();
            } elseif ($task instanceof TaskInterface) {
                $this->handle($task);
            } else {
                throw new RuntimeException('Something wrong while determine type of incoming task/job.');
            }
            // @todo add hook
            echo '[*] Completed. ' . PHP_EOL;
        } catch (\Exception $exception) {
            echo " [*] Error on: {$exception->getMessage()}\n";
            // @todo add hook
        }
    }

    public function __destruct()
    {
        try {
            if ($this->getChannel()) {
                $this->getChannel()->close();
            }

            if (self::getConnection()) {
                self::getConnection()->close();
            }
        } catch (\Exception $e) {
        }
    }
}
