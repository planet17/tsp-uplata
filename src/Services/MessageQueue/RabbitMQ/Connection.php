<?php


namespace App\Services\MessageQueue\RabbitMQ;


use PhpAmqpLib\Connection\AMQPStreamConnection;
use RuntimeException;


class Connection
{
    protected static $connection;

    protected static $user;

    protected static $password;

    protected static $host;

    protected static $port;

    public const DEFAULT_HOST = 'localhost';

    public const DEFAULT_PORT = 5672;

    /**
     * @param string  $user
     * @param string  $password
     * @param string  $host
     * @param integer $port
     */
    public static function setCredentials(
        string $user,
        string $password,
        string $host = self::DEFAULT_HOST,
        int $port = self::DEFAULT_PORT
    ):void {
        self::$user     = $user;
        self::$password = $password;
        self::$host     = $host;
        self::$port     = $port;
    }

    private static function isSetCredentials():void
    {
        if ( !self::$user || !self::$password) {
            throw new RuntimeException('Message Queue Credentials is not set');
        }
    }

    /**
     * @return AMQPStreamConnection
     */
    public static function connect():AMQPStreamConnection
    {
        self::isSetCredentials();

        if ( !self::$connection) {
            self::$connection = new AMQPStreamConnection(self::$host, self::$port, self::$user, self::$password);
        }

        return self::$connection;
    }
}
