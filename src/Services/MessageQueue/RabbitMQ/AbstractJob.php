<?php


namespace App\Services\MessageQueue\RabbitMQ;


use App\Services\MessageQueue\RabbitMQ\Interfaces\JobInterface;
use PhpAmqpLib\Message\AMQPMessage;


abstract class AbstractJob extends AbstractTask implements JobInterface
{
    abstract public function handle();

    public function __invoke()
    {
        $this->handle();
    }
}
