<?php


namespace App\Services\MessageQueue\RabbitMQ\Interfaces;


interface JobInterface extends TaskInterface
{
    public function handle();
    public function __invoke();
}
