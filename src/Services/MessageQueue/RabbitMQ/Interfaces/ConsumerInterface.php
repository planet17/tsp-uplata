<?php


namespace App\Services\MessageQueue\RabbitMQ\Interfaces;


interface ConsumerInterface
{
    public function consume():void;
}
