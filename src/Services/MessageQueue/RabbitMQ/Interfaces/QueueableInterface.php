<?php


namespace App\Services\MessageQueue\RabbitMQ\Interfaces;


use PhpAmqpLib\Connection\AMQPStreamConnection;


interface QueueableInterface
{
    public static function setConnection(AMQPStreamConnection $connection);

    public static function getConnection():AMQPStreamConnection;

    public function setQueueName(string $queueName):QueueableInterface;

    public function getQueueName():string;
}
