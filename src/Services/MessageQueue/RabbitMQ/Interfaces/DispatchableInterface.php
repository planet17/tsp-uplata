<?php


namespace App\Services\MessageQueue\RabbitMQ\Interfaces;


interface DispatchableInterface
{
    public function dispatch();
}
