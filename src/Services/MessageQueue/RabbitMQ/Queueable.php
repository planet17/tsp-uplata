<?php


namespace App\Services\MessageQueue\RabbitMQ;


use App\Services\MessageQueue\RabbitMQ\Interfaces\QueueableInterface;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;


abstract class Queueable implements QueueableInterface
{
    /**
     * @var AMQPStreamConnection $connection
     */
    private static $connection;

    /**
     * @var AMQPChannel $channel
     */
    private $channel;

    protected $queueName;

    public static function setConnection(AMQPStreamConnection $connection)
    {
        self::$connection = $connection;
    }

    public static function getConnection():AMQPStreamConnection
    {
        if (!self::$connection) {
            self::$connection = Connection::connect();
        }

        if (!self::$connection) {
            throw new \RuntimeException('Connection non initialized!');
        }

        return self::$connection;
    }

    public function setQueueName(string $queueName):QueueableInterface
    {
        $this->queueName = $queueName;

        return $this;
    }

    public function getQueueName():string
    {
        return $this->queueName;
    }

    protected function getChannel():AMQPChannel
    {
        if (!$this->channel) {
            $this->channel = self::getConnection()->channel();
        }

        return $this->channel;
    }

    protected function channelDeclaration():void
    {
        $this->getChannel()->queue_declare($this->getQueueName());
    }
}
