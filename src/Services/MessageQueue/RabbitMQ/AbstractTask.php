<?php


namespace App\Services\MessageQueue\RabbitMQ;


use App\Services\MessageQueue\RabbitMQ\Interfaces\TaskInterface;
use PhpAmqpLib\Message\AMQPMessage;


abstract class AbstractTask extends Queueable implements TaskInterface
{
    public function dispatch()
    {
        $this->channelDeclaration();
        $this->getChannel()->basic_publish(new AMQPMessage(serialize($this)), '', $this->getQueueName());
    }
}
