<?php


namespace App\Services;


use App\Entity\Post;
use Exception;
use PDO;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class DB
{
    protected static $connection;

    protected static $user;

    protected static $password;

    protected static $db;

    protected static $host;

    protected static $port;

    /**
     * @param string $user
     * @param string $password
     * @param string $dbName
     * @param string $host
     * @param int    $port
     */
    public static function setConfig(string $user, string $password, string $dbName, string $host = 'localhost', int $port = 5432):void
    {
        self::$user     = $user;
        self::$password = $password;
        self::$db       = $dbName;
        self::$host     = $host;
        self::$port     = $port;
    }

    /**
     * @return PDO
     */
    public static function getConnect()
    {
        if ( !self::$connection) {
            $host             = self::$host;
            $port             = self::$port;
            $db               = self::$db;
            $user             = self::$user;
            $pwd              = self::$password;
            self::$connection = new PDO("pgsql:host={$host};port={$port};dbname={$db};user={$user};password={$pwd}");
        }

        return self::$connection;
    }

    /**
     * @param Post $post
     *
     * @return boolean
     * @throws Exception
     */
    public static function callNewPost(Post $post)
    {
        $procedure = 'SELECT "NewPost"(?, ?, ?, ?)';
        $stmt      = self::getConnect()->prepare($procedure);

        $stmt->bindValue(1, $post->topicTitle);
        $stmt->bindValue(2, $post->body);
        $stmt->bindValue(3, $post->author);
        $stmt->bindValue(4, $post->date);

        $result = $stmt->execute();

        if (!$result) { throw new Exception($stmt->errorInfo()[2]); }

        return $result;
    }
}
