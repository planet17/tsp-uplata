<?php


namespace App\Services\Crawlers\ForumTopic;


use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


interface CrawlerMethodInterface
{
    public function run();
    public function setAccountHelper(AccountHelperInterface $helper):CrawlerMethodInterface;
    public function setUrlGenerator(callable $generator):CrawlerMethodInterface;
    public function setPagesLimit(int $limit):CrawlerMethodInterface;
    public function sendRequest(RequestInterface $request):ResponseInterface;
}
