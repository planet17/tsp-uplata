<?php


namespace App\Services\Crawlers\ForumTopic;


interface AccountHelperInterface
{
    public function setUserLogin(string $userLogin):AccountHelperInterface;

    public function setUserPassword(string $userPassword):AccountHelperInterface;

    public function getUserLogin():string;

    public function getUserPassword():string;

    public function setLoginFormActionLink(string $link):AccountHelperInterface;

    public function getLoginFormActionLink():string;
}
