<?php


namespace App\Services\Crawlers\ForumTopic;


use Exception;
use GuzzleHttp\{Client, ClientInterface, Cookie\CookieJar};
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\{RequestInterface, ResponseInterface};
use RuntimeException;
use function App\ddTrace;
use function GuzzleHttp\Psr7\parse_header;


/**
 * Pattern "Template Method"
 *
 * @package App\Services\Crawlers\ForumTopic
 */
abstract class CrawlerMethod implements CrawlerMethodInterface
{
    protected $linkBase;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var boolean
     */
    protected $isLogin = false;

    /**
     * @var AccountHelperInterface $accountHelper
     */
    protected $accountHelper;

    /**
     * @var callable $urlGenerator
     */
    protected $urlGenerator;

    protected $pagesLimit = 0;

    /**
     * @var ResponseInterface $loginResponse
     */
    protected $loginResponse;

    protected const DEFAULT_CHARSET    = 'UTF-8';

    protected const DEFAULT_USER_AGENT = 'Mozilla/5.0 TSParser/0.9.1';

    /**
     * @var CookieJar
     */
    protected $cookieJar;

    /**
     * Core method for initialize Crawler and dispatch generated requests.
     *
     * @throws Exception
     */
    final public function run():void
    {
        $this->isValidConfigure();
        $this->prepareClient();
        if ($this->isLogin && $this->accountHelper) {
            $this->loginRequest();
            $this->validateLoginResponseStatus();
            $this->validateLoginResponseBody();
        }

        foreach (($this->urlGenerator)($this->pagesLimit) as $request) {
            $this->dispatchRequestProcessor($request);
        }
    }

    abstract protected function dispatchRequestProcessor(RequestInterface $request):void;

    final private function isValidConfigure():void
    {
        if ($this->isLogin && !$this->accountHelper) {
            throw new RuntimeException('AccountHelper must be configured while isLogin equals true');
        }
    }

    protected function prepareClient():void
    {
        $this->client = new Client($this->getClientConfig());
    }

    protected function getClientConfig():array
    {
        return [
            'allow_redirects' => false,
            'base_uri'        => $this->getLinkBase(),
            'headers'         => ['user-agent' => $this->getUserAgent()],
        ];
    }

    final protected function getLinkBase():string
    {
        return $this->linkBase;
    }

    protected function getUserAgent():string
    {
        return self::DEFAULT_USER_AGENT;
    }

    public function setAccountHelper(AccountHelperInterface $helper):CrawlerMethodInterface
    {
        $this->accountHelper = $helper;

        return $this;
    }

    protected function loginRequest():void
    {
        $this->cookieJar     = new CookieJar();
        $request             = new Request('POST', $this->accountHelper->getLoginFormActionLink());
        $this->loginResponse = $this->client->send($request, array_merge($this->prepareFormData(), ['cookies' => $this->cookieJar]));
    }

    protected function prepareFormData():array
    {
        return ['form_params' => $this->accountHelper->forgeLoginFormData()];
    }

    /**
     * @throws Exception
     */
    protected function validateLoginResponseStatus():void
    {
    }

    /**
     * @throws Exception
     */
    protected function validateLoginResponseBody():void
    {
    }

    public function sendRequest(RequestInterface $request):ResponseInterface
    {
        $options = [];

        if ($this->cookieJar) {
            $options['cookie'] = $this->cookieJar;
        }

        return $this->client->send($request, $options);
    }

    public function getCharset():string
    {
        return self::DEFAULT_CHARSET;
    }

    public function extractBodyFromResponse(ResponseInterface $response)
    {
        $content       = $response->getBody()->getContents();
        $type          = $response->getHeader('content-type');
        $parsed        = parse_header($type);
        $originCharset = $parsed[0]['charset'] ?: self::DEFAULT_CHARSET;
        if ($originCharset !== $this->getCharset()) {
            $content = mb_convert_encoding($content, $this->getCharset(), $originCharset);
        }

        return $content;
    }

    public function setUrlGenerator(callable $generator):CrawlerMethodInterface
    {
        $this->urlGenerator = $generator;

        return $this;
    }

    public function setPagesLimit(int $limit):CrawlerMethodInterface
    {
        $this->pagesLimit = $limit;

        return $this;
    }

    public function __sleep()
    {
        $this->urlGenerator = null;
        $this->client = null;
        return array_keys(get_object_vars($this));
    }

    public function __wakeup()
    {
        $this->prepareClient();
    }
}
