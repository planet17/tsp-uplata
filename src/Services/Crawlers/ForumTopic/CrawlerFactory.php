<?php

namespace App\Services\Crawlers\ForumTopic;


class CrawlerFactory
{
    protected $crawlerMethodClass;

    protected $accountHelperClass;

    protected $targetTopicUrl;

    protected $userPassword;

    protected $userLogin;

    /** @var integer */
    protected $pagesLimit = 0;


    public function factory():CrawlerMethodInterface
    {
        /** @var CrawlerMethodInterface $crawlerMethod */
        $crawlerMethod = new $this->crawlerMethodClass;

        if ($this->accountHelperClass) {
            $crawlerMethod->setAccountHelper($this->accountHelperFactory());
        }

        $crawlerMethod->setUrlGenerator(
            (new UrlGeneratorFactory)->factory($this->targetTopicUrl, $this->pagesLimit)
        );

        $crawlerMethod->setPagesLimit($this->pagesLimit);

        return $crawlerMethod;
    }


    private function accountHelperFactory():AccountHelperInterface
    {
        /** @var AccountHelperInterface $accountHelper */
        $accountHelper = new $this->accountHelperClass;
        $accountHelper->setUserLogin($this->userLogin)
                      ->setUserPassword($this->userPassword);

        return $accountHelper;
    }

    /**
     *
     * @param string $crawlerMethodClass
     *
     * @return $this
     */
    public function setCrawlerMethodClass(string $crawlerMethodClass):CrawlerFactory
    {
        if ( !is_a($crawlerMethodClass, CrawlerMethodInterface::class, true)) {
            throw new \InvalidArgumentException('Crawler not implement interface: ' . CrawlerMethodInterface::class);
        }

        $this->crawlerMethodClass = $crawlerMethodClass;

        return $this;
    }

    /**
     *
     * @param $accountHelperClass
     *
     * @return $this
     */
    public function setAccountHelperClass(string $accountHelperClass):CrawlerFactory
    {
        if ( !is_a($accountHelperClass, AccountHelperInterface::class, true)) {
            throw new \InvalidArgumentException('Crawler not implement interface: ' . CrawlerMethodInterface::class);
        }

        $this->accountHelperClass = $accountHelperClass;

        return $this;
    }

    public function setTargetTopicUrl(string $targetTopicUrl):CrawlerFactory
    {
        $this->targetTopicUrl = $targetTopicUrl;

        return $this;
    }

    public function setUserPassword(string $userPassword):CrawlerFactory
    {
        $this->userPassword = $userPassword;

        return $this;
    }

    public function setUserLogin(string $userLogin):CrawlerFactory
    {
        $this->userLogin = $userLogin;

        return $this;
    }

    public function setPagesLimit(int $limit):CrawlerFactory
    {
        $this->pagesLimit = $limit;

        return $this;
    }
}
