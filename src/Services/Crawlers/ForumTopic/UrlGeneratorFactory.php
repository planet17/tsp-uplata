<?php


namespace App\Services\Crawlers\ForumTopic;


use GuzzleHttp\Psr7\Request;

class UrlGeneratorFactory
{
    protected $keyParam = '&page=';

    public function factory(string $target, $defaultLimit):callable
    {
        $keyParam = $this->keyParam;

        $requests = static function($total) use ($defaultLimit, $target, $keyParam) {
            if ( !$total && $defaultLimit) {
                $total = $defaultLimit;
            }

            $url = $target;
            for ($i = 0; $i < $total; $i++) {
                if ($i > 1) {
                    $url .= $keyParam . $i;
                }

                yield new Request('GET', $url);
            }
        };

        return $requests;
    }
}
