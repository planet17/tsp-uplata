<?php


namespace App\Services\Crawlers\ForumTopic;


abstract class AccountHelper implements AccountHelperInterface
{
    protected $userLogin;

    protected $userPassword;

    protected $formActionLink;

    abstract public function forgeLoginFormData():array;

    public function setUserLogin(string $userLogin):AccountHelperInterface
    {
        $this->userLogin = $userLogin;

        return $this;
    }

    public function setUserPassword(string $userPassword):AccountHelperInterface
    {
        $this->userPassword = $userPassword;

        return $this;
    }

    public function getUserLogin():string
    {
        return $this->userLogin;
    }

    public function getUserPassword():string
    {
        return $this->userPassword;
    }

    public function setLoginFormActionLink(string $link):AccountHelperInterface
    {
        $this->formActionLink = $link;

        return $this;
    }

    public function getLoginFormActionLink():string
    {
        return $this->formActionLink;
    }
}
