<?php


namespace App\Queue\Consumers;


use App\Services\MessageQueue\RabbitMQ\Worker;

class Request extends Worker
{
    protected $queueName = 'request';
}
