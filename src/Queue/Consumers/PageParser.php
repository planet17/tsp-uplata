<?php


namespace App\Queue\Consumers;


use App\Services\MessageQueue\RabbitMQ\Worker;


class PageParser extends Worker
{
    protected $queueName = 'pageParser';
}
