<?php


namespace App\Queue\Jobs;


use App\Entity\Post;
use App\Services\DB;
use App\Services\MessageQueue\RabbitMQ\AbstractJob;


class PostStoringJob extends AbstractJob
{
    protected $queueName = 'postStoring';

    /**
     * @var Post $post
     */
    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        DB::callNewPost($this->post);
    }
}
