<?php

namespace App\Queue\Jobs;


use App\Services\Crawlers\ForumTopic\CrawlerMethodInterface;
use App\Services\MessageQueue\RabbitMQ\AbstractJob;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\RequestInterface;


class RequestJob extends AbstractJob
{
    protected $queueName = 'request';

    /**
     * @var CrawlerMethodInterface
     */
    private $crawlerMethod;

    /**
     * @var RequestInterface
     */
    private $request;


    public function __construct(CrawlerMethodInterface $crawlerMethod, RequestInterface $request)
    {
        $this->crawlerMethod = $crawlerMethod;
        $this->request = $request;
    }

    public function handle()
    {
        $response = $this->crawlerMethod->sendRequest($this->request);
        $content  = $this->crawlerMethod->extractBodyFromResponse($response);
        (new PageParseJob($content, $this->crawlerMethod->getCharset()))->dispatch();
    }
}
