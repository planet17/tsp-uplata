<?php


namespace App\Queue\Jobs;


use App\Builders\PostBuilder;
use App\Entity\Post;
use App\Services\MessageQueue\RabbitMQ\AbstractJob;
use Symfony\Component\DomCrawler\Crawler;


class PageParseJob extends AbstractJob
{
    protected $queueName = 'pageParser';

    protected $content;

    protected $charset;


    public function __construct(string $content, string $charset)
    {
        $this->content = $content;
        $this->charset = $charset;
    }

    public function handle()
    {
        $crawler = new Crawler(null, null);

        $crawler->addHtmlContent($this->content, $this->charset);

        $pageTitle    = trim($crawler->filter('title')->text());
        $postsCrawler = $crawler->filter('div#postlist ol#posts li[id^=\'post_\']');

        $builder = (new PostBuilder())->create();
        $result  = $postsCrawler->each(
            static function($node) use ($builder, $pageTitle) {
                /** @var Crawler $node */
                $body = $node->filter('div.content div[id^=\'post_message\'] blockquote')->html();
                $body = trim(str_replace(["\t", "\n"], ' ', $body));
                while (strpos('  ', $body) !== false) {
                    str_replace('  ', ' ', $body);
                }

                try {
                    $title = trim($node->filter('div.postrow h2.title')->text());
                } catch (\Exception $e) {
                    $title = $pageTitle;
                }

                $date = trim($node->filter('span.postdate.old')->text());
                $date = date('Y-m-d H:i:s', strtotime($date));

                $builder
                    ->setDate($date)
                    ->setAuthor(trim($node->filter('div.username_container a.username strong')->text()))
                    ->setBody($body)
                    ->setTopicTitle($title);

                return $builder->get();
            });

        foreach ($result as $item) {
            $this->itemProcessing($item);
        }
    }

    protected function itemProcessing(Post $item):void
    {
        (new PostStoringJob($item))->dispatch();
    }
}
