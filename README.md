## Uplata ##

### Specification: ###

Реализовать функционал на базе PHP 7.2+, без каких-либо фреймворков, можно
использовать библиотеки для парсинга страниц и работы с RabbitMQ через composer.

В первую очередь важна архитектура и оформление кода.

Задача:

    • Реализовать авторизацию на сайте http://forumodua.com/

    • Реализовать парсинг десяти страниц темы с разбором всех сообщений

    • Создать таблицу в БД PostgreSQL для хранения всех сообщений
    (заголовок, автор, дата, текст)

    • Создать хранимую процедуру в БД для добавления данных в таблицу

    • Сохранять каждое сообщение в таблицу используя хранимую процедуру (с доп.)

    • Для сохранения данных нужно использовать менеджер очередей RabbitMQ,
    структура очереди должна быть максимально эффективна

Для создания данного парсера нужно использовать шаблон проектирования,
чтоб была возможность в дальнейшем расширять его -
добавить дополнительный форум для парсинга при минимальной затрате времени.

Логин-пароль от сайта, адреса тем, форм авторизации и т. п.
должны быть вынесены в отдельный файл конфигурации.

### Requirements: ###

        PHP: >= 7.2
        PostgreSQL: tested on 10.10 
        RabbitMQ: tested on 3.6.10

### Installation ###

1) After clone project use `composer install` for clean triggers all command.
2) After installing project configure all configurations files at dir `config`.
Files must be created while composer installing. File created with templates with post-fix `dist` in the name.
Configure file Forum for set target of parsing.
3) Create Table and Stored Procedure at your PostgreSQL Database with following 2 scripts:

    ```postgresql
    CREATE TABLE posts (
        topic_title  varchar(255),
        body   varchar(20000) NOT NULL,
        author varchar(64)    NOT NULL,
        date   timestamp      NOT NULL
    );
    ``` 

    ```postgresql
    CREATE FUNCTION NewPost(_topic_title char, _body char, _author char, _date timestamp)
        RETURNS void AS
    $BODY$
    BEGIN
    INSERT INTO posts(topic_title, body, author, date)
    VALUES(_topic_title, _body, _author, _date);
    END;
    $BODY$
        LANGUAGE 'plpgsql' VOLATILE;
    ``` 

4) For initialize Message Queue Consumers (Workers) must been initialize three files from directory `public/consumers`.
Supervisord can be used for managed all, or run it manually in different Terminal.

5) For run parsing - run script `public/index.php`
